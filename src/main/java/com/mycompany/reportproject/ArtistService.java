/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reportproject;

import java.util.List;

/**
 *
 * @author anawi
 */
public class ArtistService {
    public List<ArtistReport> getToptenArtistByTotalPrice(){
        ArtistDao dao = new ArtistDao();
        return dao.getArtistByTotalPrice(10);
    }
    public List<ArtistReport> getToptenArtistByTotalPrice(String begin,String end){
        ArtistDao dao = new ArtistDao();
        return dao.getArtistByTotalPrice(begin,end,10);
    }
}
